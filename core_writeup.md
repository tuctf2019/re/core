# Core Writeup

## Setup

They are given a `core` dump of a binary and the `core.c` code.

They are unaware of what data was sent to the binary and therefore must RE how the binary works to determine what data was sent and how to extract it.

## Solution

1. read the C code and realize that 2 inputs are sent
	* input1: the flag (which gets XOR'd)
	* input2: useless garbage to create the core dump (ignore it)
1. NOTE: The core dump has this data somewhere in it (core dumps are mem dumps when a pgrm crashes)
1. Since the program XOR's the flag sent to it, and you know it starts with `TUCTF`, they should use `strings` on the binary and look for `UTBUGz`.
1. Then extract the full string and XOR that to get the flag.

