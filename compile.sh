#!/bin/bash
#   -- Script Args --
# $1: 1 to run, anything else to not run
# $2: 0 to not compile, anything else to compile

# The Binary to be executed
bin=run
# Function that makes it easy to append arguments
addArg() { argv="$argv $@"; }
# Add an argument if it passes a condition
addArgCon() {
	con=$1 # the condition to check
	val=$2 # the value the condition needs to be
	args=${@:3} # get all other arguments to add
	if [ "$con" -eq "$val" ]; then
		addArg "$args"
	fi
}


#	Settings
x86=0
bytealign=0
pie=1
dep=1
pic=1
canary=0


# 32 bit
addArgCon $x86 1 -m32
# Changes byte alignment to 4 instead of 16 (helps for pwning)
addArgCon $bytealign 1 -mpreferred-stack-boundary=2
# No Position Independent Execution
addArgCon $pie 0 -no-pie
# No DEP / no NX
addArgCon $dep 0 -z execstack
# No Position Independent Code
addArgCon $pic 0 -fno-pic
# Stack Canary
addArgCon $canary 1 -fstack-protector-all
# Disable Stack Canary
addArgCon $canary 0 -fno-stack-protector


# Do we not compile?
if [[ -z "$2" ]] || [[ "$2" -ne 0 ]]; then
    # Del old binary, compile new one
    echo -e "Compiling '$bin' with: $argv"
    rm "$bin" >/dev/null 2>&1
    gcc $argv ./$bin.c -o $bin
    err=$?
    # Quit if gcc fails
    if [ "$err" -ne 0 ]; then
	tput setaf 1
	>&2 echo -e "\tgcc failed to compile!"
	tput setaf 15
	exit
    fi
fi

# Do we run?
if [[ ! -z "$1" ]] && [[ "$1" -eq 1 ]]; then
    echo "___________________________________________"
    ./$bin
    echo "___________________________________________"
fi
