# Core

Desc: `We were able to recover a few files we need analyzed. Think you can get anything outta these?`

Given files:

* core
* run.c

Hints:

* How is the data manipulated before the crash? Do you know any of the characters?

Flag: `TUCTF{c0r3_dump?_N3v3r_h34rd_0f_y0u}`
